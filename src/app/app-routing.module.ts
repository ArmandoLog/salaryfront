import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalaryFormComponent } from './salary/salary-form/salary-form.component';
import { SalaryTableComponent } from './salary/salary-table/salary-table.component';

const routes: Routes = [
  {path:'', component: SalaryFormComponent},
  {path:'table', component: SalaryTableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
}
