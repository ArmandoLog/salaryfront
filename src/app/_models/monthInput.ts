export interface MonthInput{
    employeeName: string,
    employeeSurname: string, 
    year: number
}