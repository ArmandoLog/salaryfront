export interface EmployeeInfo {
    employeeName: string,
    employeeSurname: string,
    employeeCode: string,
    identificationNumber: string, 
    year: number, 
    month: number, 
    birthdate: Date, 
    beginDate: Date
}
