export interface SalaryInpunt {
    employeeName: string,
    employeeSurName: string,
    identificationNumber: string
    employeeCode: string,
    beginDate: Date,
    birthdate: Date,
    variableData: VariableData[]    
}
export interface VariableData{
    year: any,
    month: any,
    officeId: any, 
    divisionId: any, 
    positionId: any, 
    grade: any,
    baseSalary: any, 
    productionBonus: any, 
    compensationBonus: any, 
    commission: any, 
    contributions: any
}
export interface SalaryRow { 
    employeeCode: string, 
    employeeFullName: string, 
    divisionName: string, 
    positionName: string, 
    grade: number, 
    beginDate: Date,
    birthDate: Date, 
    identificationNumber: string,
    totalSalary: number,
    officeId: number
}
