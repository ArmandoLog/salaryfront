import { Component, OnInit } from '@angular/core';
import { SalaryService } from 'src/app/services/salary.service';
import { SalaryInpunt, VariableData } from 'src/app/_models/salary';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { EmployeeInfo } from 'src/app/_models/employeeInfo';
import { MonthInput } from 'src/app/_models/monthInput';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-salary',
  templateUrl: './salary-form.component.html',
  styleUrls: ['./salary-form.component.css']
})

export class SalaryFormComponent implements OnInit {
  salaryForm!: FormGroup;
  salaryInput!: SalaryInpunt;

  offices = [
    { id:1, name: 'Z'},
    { id:2, name: 'ZZ'},
    { id:3, name: 'D'},
    { id:4, name: 'C'},
    { id:5, name: 'B'},
    { id:6, name: 'A'},
  ];

  divisions =[
    { id:1, name: 'MARKETING'},
    { id:2, name: 'IT'},
    { id:3, name: 'SALES'},
    { id:4, name: 'OPERATIONS'},
    { id:5, name: 'CUSTOMER CARE'},
  ];

  positions =[
    { id:1, name: 'MARKETING MANAGER'},
    { id:2, name: 'CUSTOMER ASSISTANT'},
    { id:3, name: 'CUSTOMER DIRECTOR'},
    { id:4, name: 'TECHNICAL LEADER'},
    { id:5, name: 'PROGRAMMER'},
    { id:6, name: 'ACCOUNT EXECUTIVE'},
    { id:7, name: 'SALES MANAGER'},
    { id:8, name: 'CARGO ASISTANT'},
    { id:9, name: 'HEAD OF CARGO'},
    { id:10, name: 'MARKETING ASISTANT'},
    { id:11, name: 'CARGO MANAGER'}
  ];

  grades = [2,4,6,8,10,12,14,16,18,20];

  years : number[] = [2019,2020,2021];
  isYearSelected: boolean = false;
  yearSelected!: number;
  
  availableMonths: any[]= [];
  yearMonths = new Map([
    [1, 'Enero'],
    [2,'Febrero'],
    [3, 'Marzo'],
    [4, 'Abril'],
    [5, 'Mayo' ],
    [6, 'Junio'],
    [7, 'Julio'],
    [8, 'Agosto'],
    [9, 'Septiembre'],
    [10, 'Octubre'],
    [11, 'Noviembre'],
    [12, 'Diciembre'],
  ]);

  employeesInfo!: EmployeeInfo[];
  employeesInfoSelected!: EmployeeInfo;
  isEmployeeSelected: boolean = false;

  names: string[]= [];

  constructor(
    private salaryService: SalaryService,
    private fb: FormBuilder,
    private toastr: ToastrService
    ) { }

  ngOnInit(): void {
    this.getEmployeesInfo();
    this.initForm();
  }

  getEmployeesInfo(){
    this.salaryService.getEmployeesInfo().subscribe(response => {
        this.employeesInfo = response;
        this.names = this.employeesInfo.map(eInfo => {
            return eInfo.employeeName + ' ' + eInfo.employeeSurname
          }
        );
    });
  }

  employeeSelected(event: any){
    this.isYearSelected = false;
    const name: string = event.option.value;

    this.employeesInfo.forEach(employeeInfo => {
      const fullName = employeeInfo.employeeName + ' ' + employeeInfo.employeeSurname;
      if(fullName === name){
        this.employeesInfoSelected = employeeInfo;
      }
    });

    this.setEmpInfoControls();
    this.isEmployeeSelected = true;
  }

  initForm(){
    this.salaryForm = this.fb.group({
      employeeName: '',
      employeeSurName: '',
      identificationNumber: '',
      employeeCode: '',
      beginDate: '',
      birthdate: '',
      variableData: this.fb.array([ ])
    });
  }

  setEmpInfoControls(){
    this.salaryForm.controls['employeeName']
    .setValue(this.employeesInfoSelected?.employeeName);

    this.salaryForm.controls['employeeSurName']
    .setValue(this.employeesInfoSelected?.employeeSurname);

    this.salaryForm.controls['identificationNumber']
    .setValue(this.employeesInfoSelected?.identificationNumber);

    this.salaryForm.controls['employeeCode']
    .setValue(this.employeesInfoSelected?.employeeCode);

    this.salaryForm.controls['beginDate']
    .setValue(this.employeesInfoSelected?.beginDate);

    this.salaryForm.controls['birthdate']
    .setValue(this.employeesInfoSelected?.birthdate);

    this.salaryForm.controls['employeeCode']
    .setValue(this.employeesInfoSelected?.employeeCode);
  }

  get variableData(){
    return  this.salaryForm.get('variableData') as FormArray;
  }

  onSelectedYear(event?: any){

    if(event){
      this.yearSelected = event.source.value;
    }

    this.isYearSelected= false;
    this.availableMonths = [];
    this.variableData.clear();

    const monthInput: MonthInput = {
      employeeName: this.employeesInfoSelected?.employeeName,
      employeeSurname: this.employeesInfoSelected?.employeeSurname,
      year:  this.yearSelected
    }
 
    this.salaryService.getAvailableMonths(monthInput)
     .subscribe(availableMonthsResponse=> {
        availableMonthsResponse.forEach(month => {
            this.availableMonths.push(month);
        })
        this.fillVaribleData();
     } );
     
     this.isYearSelected = true;
  }

  fillVaribleData(){
  this.availableMonths.forEach(month => {
      this.variableData.push(this.fb.group({
        year:  this.yearSelected,
        month: month, 
        officeId: ['', Validators.required],
        divisionId:['', Validators.required], 
        positionId:['', Validators.required], 
        grade: ['', Validators.required], 
        baseSalary: ['', Validators.required], 
        productionBonus: ['', Validators.required], 
        compensationBonus: ['', Validators.required], 
        contributions: ['', Validators.required],
        commission: ['', Validators.required]
      })) 
  });
  }
   
   sendSalaries(){
        let notPristineForms = this.fb.array([]);
        this.variableData.controls.forEach((control, i) => {
          const monthForm = control as FormGroup;
          if(!monthForm.pristine){
            notPristineForms.push(monthForm);
          }
        })

      this.variableData.controls = notPristineForms.controls;
      
      let monthIncompleted: any[] = [];
      let isInvalidForm: boolean = false;

      this.variableData.controls.forEach(control => {
        const monthForm = control as FormGroup;
        if(!monthForm.valid){
          monthIncompleted.push(monthForm.get('month')?.value);
          isInvalidForm = true;
        }
      })

      if(isInvalidForm || this.availableMonths.length <=0){
        for(let i= 0; i<monthIncompleted.length; i++){
          monthIncompleted[i]  = this.yearMonths.get(monthIncompleted[i]);
        }

        this.toastr
        .error(
          'Los siguientes meses estan incompletos: ' + monthIncompleted +
          '. Recuerda que todos los campos son requeridos'
        );
        this.onSelectedYear();
        
      }else {
        console.log('is valid')
        console.log()

        let variableDataArray: VariableData[] = [];
        this.variableData.controls.forEach(monthForm => {
          variableDataArray.push({
            year: monthForm.get('year')?.value,
            month: monthForm.get('month')?.value,
            officeId: monthForm.get('officeId')?.value, 
            divisionId: monthForm.get('divisionId')?.value, 
            positionId: monthForm.get('positionId')?.value, 
            grade: monthForm.get('grade')?.value,
            baseSalary: monthForm.get('baseSalary')?.value, 
            productionBonus: monthForm.get('productionBonus')?.value, 
            compensationBonus: monthForm.get('compensationBonus')?.value, 
            commission: monthForm.get('commission')?.value, 
            contributions: monthForm.get('contributions')?.value
          })
        }

        )
        this.salaryInput = {
          employeeName: this.salaryForm.get('employeeName')?.value,
          employeeSurName: this.salaryForm.get('employeeSurName')?.value,
          identificationNumber: this.salaryForm.get('identificationNumber')?.value,
          employeeCode: this.salaryForm.get('employeeCode')?.value,
          beginDate: this.salaryForm.get('beginDate')?.value,
          birthdate: this.salaryForm.get('birthdate')?.value,
          variableData: variableDataArray
        }

        this.salaryService.addSalaries(this.salaryInput).subscribe(
          response => {
            console.log('ADD SALARIES RESPONSE', response);
            if(response){
              this.toastr.success('Salario/s actualizado/s con exito');
              this.onSelectedYear();
            } else {
              this.toastr.error('Fallo en actualizacon de salario/s');
            }
          },error => {
            this.toastr.error('Fallo en actualizacon de salario/s');
            console.log('error: ', error.message);
          }

        )
      }//ELSE 


      // this.salaryForm.controls['variableData'] = this.fb.array([])
        // this.salaryForm.controls['variableData'] = notPristineForms;
   }
}
   