import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalaryFormComponent } from './salary-form/salary-form.component';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SalaryTableComponent } from './salary-table/salary-table.component';

@NgModule({
  declarations: [
    SalaryFormComponent,
    SalaryTableComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule, 
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ]
})
export class SalaryModule { }
