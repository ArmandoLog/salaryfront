import { Component, OnInit } from '@angular/core';
import { SalaryService } from 'src/app/services/salary.service';
import { SalaryRow } from 'src/app/_models/salary';

@Component({
  selector: 'app-salary-table',
  templateUrl: './salary-table.component.html',
  styleUrls: ['./salary-table.component.css']
})
export class SalaryTableComponent implements OnInit {

  rows: SalaryRow[] = [];
  isRowSelected: boolean = false;
  selectedRow: SalaryRow | undefined;
  resetDisabled: boolean = true;
  rowId: number = -1;

  constructor(private salaryService: SalaryService) { }

  ngOnInit(): void {
    this.getSalaries();
  }

  getSalaries(){
    this.salaryService.getSalaries().subscribe((response: any)=> {
      this.rows = response;
    })
  }

  onSelectSalary(elementId: number, row: any){
    if(this.isRowSelected && this.rowId === elementId){
      this.rowId = -1;
      this.selectedRow = undefined;
      this.isRowSelected = false;
    }else {
      this.rowId = elementId;
      this.selectedRow = row;
      this.isRowSelected = true;
    }

  }

  sameGrade(){
    if(this.selectedRow){
      this.rows = this.rows
        .filter(row => row.grade === this.selectedRow?.grade);
        this.resetDisabled = false;
        this.isRowSelected = false;
    }
  }

  samePositionGrade(){
    if(this.selectedRow){
        this.rows = this.rows
          .filter(row => 
            row.positionName === this.selectedRow?.positionName && row.grade === this.selectedRow?.grade
            );
            this.resetDisabled = false;
            this.isRowSelected = false;
    }
  }

  sameOfficePosition(){
    if(this.selectedRow){
      this.rows = this.rows
        .filter(row => 
          row.grade === this.selectedRow?.grade && row.officeId === this.selectedRow?.officeId
          );
          this.resetDisabled = false;
          this.isRowSelected = false;
    }
  }

  reset(){
    this.getSalaries();
    this.resetDisabled = true;
  }

}

/**NOTE : 
 Los otros comentarios en el codigo estan en ingles por que acostumbro a hacerlos asi y sin darme cuenta
 empece a hacerlos en ingles aqui tambien, asi que por coherencia segui haciendolos en ingles, este comentario
 sin embargo lo hare en español por practicidad.

 las funciones de filtro las hice siguiendo las siguientes reglas
 * 
  1-Empleados con la misma Oficina y Grado. (misma oficina y grado) 
  2- Empleados de todas las Oficinas y con el mismo Grado. (mismo grado)
  3- Empleados con la misma Posición y Grado. ( misma posicion y grado)
  4- Empleados de todas las Posiciones y con el mismo Grado. (mismo grado)
 * 
  me di cuenta de que las instrucciones son reduntantes ya que la 2 y la 4 son las mismas 
  (segun como las entendi), estoy consciente de que puede haber mal entendido las reglas y se me ocurre que por ejemplo la 2 pudiera
   significar que la tabla muestre todas las filas (todas las officinas, incluyendo las que tienen el mismo grado y las que no) y 
  se le agregue ademas las que tengan en mismo grado, repitiendo asi dos veces las que tengan el mismo grado, si es asi
  eso lo puede haber hecho con la funcion concat: 

  this.rows.concat(this.rows
        .filter(row => 
          row.officeId === this.selectedRow?.officeId && row.grade === this.selectedRow?.grade
          ));
 
  este comentario es solo para dejar claro que tenia esa idea si se diera el caso 

 */
