import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { EmployeeInfo } from '../_models/employeeInfo';
import { MonthInput } from '../_models/monthInput';
import { SalaryInpunt } from '../_models/salary';

@Injectable({
  providedIn: 'root'
})
export class SalaryService {

  baseUrl = environment.apiUrl + 'salary/';

  constructor(private http: HttpClient) { }

  getSalaries() {
    return this.http.get(this.baseUrl+'all');
  }

  getEmployeesInfo(){
    return this.http.get<EmployeeInfo[]>(this.baseUrl + 'employees-info');
  }

  getAvailableMonths(monthInput: MonthInput){
    const httpParmas = new HttpParams({
      fromObject: {
        employeeName: monthInput.employeeName,
        employeeSurname: monthInput.employeeSurname,
        year: monthInput.year
      }
    })

    return this.http.get<number[]>(this.baseUrl + 'available-months', {params: httpParmas});
  }

  addSalaries(salaryInput: SalaryInpunt){
    return this.http.post(this.baseUrl + 'add-salaries', salaryInput);
  }
}
